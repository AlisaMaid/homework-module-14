#include <iostream>
#include <string>

int main()
{
    std::string text = "This is a text!";

    std::cout << text << "\n";
    std::cout << "Length = " << text.length() << "\n";
    std::cout << "First symbol is '" << text[0] << "'\n";
    std::cout << "Last symbol is '" << text[text.length()-1] << "'\n";
}
